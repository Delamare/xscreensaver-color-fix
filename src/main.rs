use std::io::{BufRead, BufReader};
use std::process::{Command, Stdio};
use std::thread::sleep;
use std::time::Duration;

fn apply_cc(commands: &mut Vec<Command>) {
    for command in commands.iter_mut() {
        if command
            .output()
            .expect("Failed to run cc command!")
            .status
            .success()
        {
            println!("Successfully set cc!");
        } else {
            eprintln!("Failed to set cc!");
        }
    }
}

fn create_cc_commands() -> Vec<Command> {
    (1..4)
        .map(|i| {
            let i = i.to_string();
            let mut path = dirs::home_dir().unwrap();
            path.push(".local/share/icc");
            let mut files: Vec<String> = std::fs::read_dir(path)
                .unwrap()
                .filter_map(|f| f.ok())
                .map(|f| f.path().display().to_string())
                .filter(|f| f.contains(format!("#{}", i).as_str()))
                .collect();
            files.sort();
            let file = files
                .iter()
                .last()
                .expect(format!("Failed to find an icc for display {}!", i).as_str());
            println!("Using file: {}", &file);
            let mut command = Command::new("dispwin");
            command.args(&mut ["-d", i.as_str(), file.as_str()]);
            command
        })
        .collect()
}

fn main() {
    let mut cc_commands: Vec<Command> = create_cc_commands();

    let mut watch_command = Command::new("xscreensaver-command");
    watch_command.stdout(Stdio::piped());
    watch_command.arg("-watch");

    let process = watch_command
        .spawn()
        .expect("Failed to run xscreensaver-command!");
    let mut reader = BufReader::new(process.stdout.expect("Failed to acquire stdout!"));
    let mut line = String::new();
    while reader.read_line(&mut line).is_ok() {
        println!("Got: {}", line);
        if line.contains("UNBLANK") {
            sleep(Duration::from_secs(1));
            apply_cc(&mut cc_commands);
        } else if line.contains("LOCK") {
            sleep(Duration::from_secs(5));
            apply_cc(&mut cc_commands);
        }
        line.clear();
    }
}
