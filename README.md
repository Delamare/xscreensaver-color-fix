# Xscreensaver Color Calibration Fixer  
If you're like me, xscreensaver removes any color calibration effects when I am in my GNOME3 environment as soon as it locks or unlocks.  This project aims to fix that!  

This application uses `xscreensaver-command -watch` to figure out when it needs to run `dispwin` to set color calibration back on.  


It also automatically finds the latest calibration for each monitor assuming that your icc files have `#X` (where X is the display integer) somewhere in the file name.  Oh and it also assumes that your icc files are in `.local/share/icc`.  Either move them there if they aren't already there, or change my code.  It's only 70 lines.

## Depends:
* xscreensaver
* dispwin

## Build Depends:
* rust
